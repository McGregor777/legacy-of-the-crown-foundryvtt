import {LotC} from "./modules/config.js";
import LotCCharacter from "./modules/actors/LotCCharacter.js"
import LotCItem from "./modules/items/LotCItem.js"
import LotCCharacterSheet from "./modules/sheets/LotCCharacterSheet.js";
import LotCItemSheet from "./modules/sheets/LotCItemSheet.js";
import * as handlebarsHelpers from "./modules/handlebars.js";
import * as hooks from "./modules/hooks.js";

async function preloadHandlebarsTemplates()
{
	const templatePaths =
	[
		"systems/lotc/templates/partials/characteristic-div-partial.html",
		"systems/lotc/templates/partials/resource-div-partial.html",
		"systems/lotc/templates/partials/progression-resource-div-partial.html",
		"systems/lotc/templates/partials/item-item-partial.html",
		"systems/lotc/templates/partials/item-weapon-partial.html",
		"systems/lotc/templates/partials/item-armour-partial.html",
		"systems/lotc/templates/partials/proficiency-row.html",
		"systems/lotc/templates/partials/skills-table-partial.html",
		"systems/lotc/templates/partials/item-row.html",
		"systems/lotc/templates/partials/item-skill-row.html",
		"systems/lotc/templates/partials/item-weapon-row.html",
		"systems/lotc/templates/partials/item-armour-row.html",
	];

	return loadTemplates(templatePaths);
}

Hooks.once("init", function()
{
	console.log("LotC | Initialising Legacy of the Crown - The Roleplaying Game");

	CONFIG.LotC = LotC;
	CONFIG.Actor.documentClass = LotCCharacter;
	CONFIG.Item.documentClass = LotCItem;
	
	//Actors.registerSheet("LOTC", LotCNPCSheet, {label: "NPC Sheet", types: ["npc"], makeDefault: true});
	
	preloadHandlebarsTemplates();
});

handlebarsHelpers.default();
hooks.default();