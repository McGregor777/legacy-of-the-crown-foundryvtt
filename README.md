# Legacy of the Crown - FoundryVTT

A FoundryVTT game system for Legacy of the Crown.

As Playable Characters should work as intended, being fully functional, the NPC and Creatures are yet to be completed, being usable without being totally finished.

The level of automation is at its lowest for the moment, only making sure things that needs to be calculated on the fly are computed by the system and not by the players; this will change in the future, but I can't make any promise for a specific date at the moment.

Make sure to let me know about any issue you encounter, or if you enjoy the system as it already is.

If you have questions, suggestions, or critics to address us, all our medias are displayed on our website https://legacyofthecrown.com.