import * as Helper from './helper.js';

/**
 * Setups and rolls a Skill Test.
 * 
 * @param {Object} skill The Skill item being tested.
 * @param {Object} actor The Actor owning the Skill.
 * @param {Object} data	Specific settings.
 */
export async function Test(actor, skill, data = {})
{
	// Setup default test settings.
	let rollData =
	{
		...data,
		title: data.title || skill.name,
		actor: actor,
		difficulty: 'average',
		// Critical State and proficiency modifiers.
		modifier: (actor.system.derived_characteristics.health_points.value <= 0
			? actor.system.derived_characteristics.health_points.value * 10
			: 0) + data.modifier,
		hitLocation: false,
		// Get currently selected Roll Mode.
		rollMode: game.settings.get('core', 'rollMode')
	};
	
	// Default a Brawling, Close Combat or Shoot/throw test to have hit location checked.
	if(skill.name == game.i18n.localize('SKILL.Brawling')
		|| skill.name == game.i18n.localize('SKILL.CloseCombat')
		|| skill.name == game.i18n.localize('SKILL.Shoot/Throw'))
		rollData.hitLocation = true;

	// Setup dialog data: title, template, buttons, prefilled data and callback.
	let dialogOptions =
	{
		title: rollData.title,
		template: '/systems/lotc/templates/dialogs/skill-test-dialog.html',
		// Prefilled dialog data.
		data:
		{
			difficulties: CONFIG.LotC.difficulties,
			rollModes: CONFIG.Dice.rollModes
		},
		callback: (html) =>
		{
			// When dialog confirmed, fill rollData dialog information.
			// Note that this does not execute until _setupTestDialog() has finished and the user confirms the dialog.
			rollData.difficulty = html.find('[name="test_difficulty"]').val();
			rollData.modifier = Number(html.find('[name="test_modifier"]').val());
			rollData.hitLocation = html.find('[name="test_hit_location"]').is(':checked');
			rollData.rollMode = html.find('[name="test_roll_mode"]').val();
			rollData.weapon = html.find('[name="test_weapon"]').val();

			return rollData;
		}
	};

	if(rollData.type === 'parry')
		dialogOptions.data.weapons = actor.itemTypes.weapon.filter(weapon =>
		{
			const weaponType = Helper.DetermineWeaponType(weapon);
			return weaponType == 'close_combat' || weaponType == 'fist_fight';
		});
	
	await _setupTestDialog(dialogOptions, rollData);
	
	rollData.difficultyModifier = CONFIG.LotC.difficultiesModifiers[rollData.difficulty];
	rollData.score = skill.system.true_value + rollData.difficultyModifier + rollData.modifier;
	
	const rollFormula = '1d100';
	let rollResult = await new Roll(rollFormula, rollData).roll();
	
	mergeObject(rollData, _calculateSuccessLevelAndOutcome(rollData.score, rollResult.total));

	if(rollData.type == 'parry' || rollData.type == 'dodge')
	{
		rollData.finalSuccessLevel = rollData.successLevel - rollData.opponent.successLevel;

		if(rollData.finalSuccessLevel >= 0)
			rollData.finalDamages = rollData.opponent.damages - rollData.finalSuccessLevel;
	}
	
	rollData.hitLocation = rollData.hitLocation
		? game.tables.contents.find(table => table.name == game.i18n.localize('TEST.HitLocation')).getResultsForRoll(rollResult.total)[0].text
		: null;
	
	if(rollData.damages)
		rollData.damages += rollData.successLevel;
	
	mergeObject(rollResult, rollData);
	
	let renderedRoll = await renderTemplate('/systems/lotc/templates/chat/roll/skill-test-chat-card.html', rollResult);
	
	let messageData =
	{
		actor: actor,
		speaker: ChatMessage.getSpeaker({actor}),
		content: renderedRoll
	};
	
	rollResult.toMessage(messageData, {rollMode: rollResult.data.rollMode});
}

/**
 * Calculate the rolled test Success Level and outcome.
 *
 * @param difficulty {Number}   The test's difficulty.
 * @param score {Number}   The test roll's score.
 * @returns {Object} {successLevel, outcome, critical}    Rolled test's Success Level, outcome and if the test is a critical.
 */
function _calculateSuccessLevelAndOutcome(difficulty, score)
{
	const scoreString = score.toString().split("");
	const difference = difficulty - score;
	const critical = scoreString[0] == scoreString[1] || score == 100 ? true : false;
	let successLevel = Math.floor(difficulty / 10) - Math.floor(score / 10);
	let outcome = '';
	
	if(difference >= 0)
		outcome = 'success';
	else
	{
		outcome = 'failure';
		
		if(successLevel == 0)
			successLevel = '-' + successLevel.toString();
	}
	
	return {successLevel, outcome, critical};
}

/**
 * Compares two tests then gives the final outcome.
 *
 * @param difficulty {Number}   The test's difficulty.
 * @param score {Number}   The test roll's score.
 * @returns {Object} {successLevel, outcome}    Rolled test's Success Level and outcome.
 */
function _compareTwoTests(difficulty, score)
{
	const scoreString = score.toString().split("");
	const difference = difficulty - score;
	const critical = scoreString[0] == scoreString[1] || score == 100 ? true : false;
	let successLevel = Math.floor(difficulty / 10) - Math.floor(score / 10);
	let outcome = '';

	if(difference >= 0)
	{
		if(critical)
			outcome = game.i18n.localize('TEST.CriticalSuccess');
		else
			outcome = game.i18n.localize('TEST.Success');
	}
	else
	{
		if(critical)
			outcome = game.i18n.localize('TEST.CriticalFailure');
		else
			outcome = game.i18n.localize('TEST.Failure');

		if(successLevel == 0)
			successLevel = '-' + successLevel.toString();
	}

	return {successLevel, outcome, critical};
}

async function _setupTestDialog(dialogOptions, rollData)
{
	mergeObject(dialogOptions.data, rollData);
	
	let html = await renderTemplate(dialogOptions.template, dialogOptions.data);
	
	return new Promise((resolve, reject) =>
	{
		new Dialog(
		{
			title: dialogOptions.title,
			content: html,
			buttons:
			{
				rollButton:
				{
					label: game.i18n.localize('TEST.Roll'),
					callback: html => resolve(dialogOptions.callback(html))
				}
			},
			default: 'rollButton'
		}).render(true);
	});
	reject();
}