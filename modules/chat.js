import { Test } from './test.js';

export function addChatMessageContextOptions(html, options)
{
	options.push(
	{
		name: game.i18n.localize('CHAT.InflictDamages'),
		icon: '<i class="fa-solid fa-burst"></i>',
		condition: inflictsDamages,
		callback: li => inflictDamages()
	});
}

export function onParryTestButtonClick(event)
{
	_reactiveDefenseTest('parry');
}

export function onDodgeTestButtonClick(event)
{
	_reactiveDefenseTest('dodge');
}

function _reactiveDefenseTest(type)
{
	const actor = game.user.character ?? game.canvas.tokens.controlledObjects.values().next().value.actor;
	const $testMessage = $(event.target.closest('.test-message'));

	let title = ''
	let skillName = '';

	if(type === 'parry')
	{
		title = game.i18n.localize('TEST.Parry');
		skillName = game.i18n.localize('SKILL.CloseCombat');
	}
	else if(type === 'dodge')
	{
		title = game.i18n.localize('TEST.Dodge');
		skillName = game.i18n.localize('SKILL.Dodge');
	}

	Test
	(
		actor,
		actor.itemTypes.skill.find(skill => skill.name == skillName),
		{
			title: title,
			type: type,
			opponent:
			{
				actor: $testMessage.data('actorId'),
				successLevel: $testMessage.find('.test-message-success-level').data('successLevel'),
				damages: $testMessage.find('.test-message-damages').data('damages'),
				hitLocation: $testMessage.find('.test-message-hit-location').data('hitLocation')
			}
		}
	);
}