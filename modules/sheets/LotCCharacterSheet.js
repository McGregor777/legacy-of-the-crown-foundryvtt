/**
 * Provides the data and general interaction with Actor Sheets - Abstract class.
 *
 * LotCCharacterSheet provides the general interaction and data organization shared among all actor sheets, as this is an abstract class, inherited by either Character or NPC specific actor sheet classes. When rendering an actor sheet, getData() is called, which is a large and key that prepares the actor data for display, processing the raw data and items and compiling them into data to display on the sheet. Additionally, this class contains all the main events that respond to sheet interaction in activateListeners().
 *
 * @see   LotCCharacter - Data and main computation model (this.actor)
 */
export default class LotCCharacterSheet extends ActorSheet
{
	/** @override */
	static get defaultOptions()
	{
		return mergeObject(super.defaultOptions,
		{
			template: "systems/lotc/templates/character-sheet.html",
			width: 800,
			height: 600,
			classes: ["lotc", "sheet", "actor"],
			tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "characteristics" }]
		});
	}

	/**
	 * Provides the data to the template when rendering the actor sheet
	 * This is called when rendering the sheet, where it calls the base actor class to organize, process, and prepare all actor data for display.
	 * 
	 * @returns {Object} data    Data given to the template when rendering
	 */
	getData()
	{
		const data = super.getData();
		
		data.config = CONFIG.LotC;
		data.items = this.constructItemLists(data);
		data.rollData = data.actor.getRollData();
		data.proficiencyLevels = {1: 1, 2: 2, 3: 3};
		
		// Apply a specific stylesheet depending on the Actor's class
		let cssClass = this.actor.system.identity.class.toLowerCase();
		
		if(!this.options.classes.find(element => element == cssClass))
			this.options.classes.push(cssClass);
		
		console.log(this);

		let totalSkillPoints = 0;
		Object.values(data.items.skills).forEach(group => { Object.values(group).forEach(skill => totalSkillPoints += skill.system.raw_value) });
		console.log(data.actor.name + "'s total Skill Points: " + totalSkillPoints);
		
		return data;
	}
	
	/**
	 * Returns items sorted by type.
	 *
	 * @param data {Object}   The Actor data
	 */
	constructItemLists(data)
	{
		let skills = data.items.filter(i => i.type == "skill").sort(function(a, b)
		{
			if(a.name < b.name)
				return -1;
			else if(a.name > b.name)
				return 1;
			else
				return 0;
		});
		
		let items =
		{
			skills:
			{
				physique: skills.filter(i => i.system.associated_characteristic == "physique"),
				constitution: skills.filter(i => i.system.associated_characteristic == "constitution"),
				agility: skills.filter(i => i.system.associated_characteristic == "agility"),
				intelligence: skills.filter(i => i.system.associated_characteristic == "intelligence"),
				willpower: skills.filter(i => i.system.associated_characteristic == "willpower"),
				charisma: skills.filter(i => i.system.associated_characteristic == "charisma")
			},
			passiveAbilities: data.items.filter(i => i.type == "passive_ability"),
			activeAbilities: data.items.filter(i => i.type == "active_ability"),
			proficiencies: data.items.filter(i => i.type == "proficiency"),
			items: data.items.filter(i => i.type == "item"),
			weapons: data.items.filter(i => i.type == "weapon"),
			armours: data.items.filter(i => i.type == "armour"),
			ranks: data.items.filter(i => i.type == "rank")
		};
		
		return items;
	}

	/**
	 * Activate event listeners using the prepared sheet HTML
	 *
	 * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
	 */
	activateListeners(html)
	{
		super.activateListeners(html);

		html.find('.stress_link').click(this._onStressClick.bind(this));
		html.find('.contamination_link').click(this._onContaminationClick.bind(this));
		html.find('.proficiency-create').click(this._onProficiencyCreate.bind(this));
		html.find('.proficiency_delete_link').click(this._onProficiencyDelete.bind(this));
		html.find('.item_edit_link').click(this._onItemEdit.bind(this));
		html.find('.item_delete_link').click(this._onItemDelete.bind(this));
		html.find('.item_name_link, .skill_true_value_link').mousedown(this._onItemClick.bind(this));
		html.find('.skill_raw_value_input').change(this._onChangeSkillRawValue.bind(this));
		html.find('.quantity_link').mousedown(this._onQuantityClick.bind(this));
	 }

	 _onStressClick(event)
	 {
		 this.actor.stressTest();
	 }

	 _onContaminationClick(event)
	 {
		 this.actor.contaminationTest();
	 }
	
	_onProficiencyCreate(event)
	{
		this.actor.addNewProficiency();
	 }

	 _onProficiencyDelete(event)
	 {
		 this.actor.deleteProficiency($(event.currentTarget).parents("tr").attr("data-key"));
	 }
	
	_getItemId(event)
	{
		return $(event.currentTarget).parents(".item").attr("data-item-id");
	}
	
	_onItemEdit(event)
	{
		let clickedItemId = this._getItemId(event);
		const clickedItem = this.actor.items.get(clickedItemId);
		
		return clickedItem.sheet.render(true);
	}
	
	_onItemDelete(event)
	{
		let li = $(event.currentTarget).parents(".item"), itemId = li.attr("data-item-id");
		
		renderTemplate('systems/lotc/templates/dialogs/delete-item-dialog.html').then(html =>
		{
			new Dialog(
			{
				title: "Delete Confirmation",
				content: html,
				buttons:
				{
					confirm:
					{
						icon: '<span class="fa fa-check"></span>',
						label: "Confirm",
						callback: async dlg =>
						{
							await this.actor.deleteEmbeddedDocuments("Item", [itemId]);
							li.slideUp(200, () => this.render(false));
						}
					},
					cancel:
					{
						icon: '<span class="fas fa-times"></span>',
						label: "Cancel"
					},
				},
					default: 'confirm'
			}).render(true)
		})
	}

	/**
	 * Handles Item clicks on the Character Sheet.
	 * Opens the Item's sheet on right-clik.
	 * Initiates a roll if the Item left-clicked is a Skill or a Weapon.
	 * 
	 * @param {any} event
	 */
	_onItemClick(event)
	{
		let clickedItemId = this._getItemId(event);
		let clickedItem = this.actor.items.get(clickedItemId);
		
		switch(event.button)
		{
			case 0:
				if(clickedItem.type == "skill" || clickedItem.type == "weapon")
					clickedItem.roll();
				else
					clickedItem.sheet.render(true);
				break;
			case 2:
				clickedItem.sheet.render(true);
				break;
		}
	}

	/**
	 * Handles Item quantity clicks on the Character Sheet.
	 * Increases the Item quantity on left-click, adds 10 instead of 1 if CTRL key is pressed.
	 * Decrases the Item quantity on right-click, removes 10 instead of 1 if CTRL key is pressed.
	 * 
	 * @param {any} event
	 */
	_onQuantityClick(event)
	{
		// Get clicked Item and Item's quantity
		let clickedItemId = this._getItemId(event);
		let clickedItem = this.actor.items.get(clickedItemId);
		let quantity = clickedItem.system.quantity;
		
		switch(event.button)
		{
			case 0:
				if(event.ctrlKey)
					quantity += 10;
				else
					quantity++;
				break;
			case 2:
				if(event.ctrlKey)
					quantity -= 10;
				else
					quantity--;
				if(quantity < 0)
					quantity = 0;
				break;
		}
		
		clickedItem.update({ "system.quantity": quantity });
	 }

	 /**
	  * Handles Skills raw value changes on the Character Sheet.
	  * 
	  * @param {any} event
	  */
	async _onChangeSkillRawValue(event)
	{
		event.preventDefault();
		
		let clickedItemId = $(event.currentTarget).parents("tr").attr("data-item-id");
		let clickedItem = this.actor.items.get(clickedItemId);
		
		clickedItem.update({ "system.raw_value": Number(event.target.value) })
	}
}

Actors.unregisterSheet("core", ActorSheet);
Actors.registerSheet("LOTC", LotCCharacterSheet, {label: "Character Sheet", makeDefault: true});