import * as Test from "../test.js";

export default class LotCItemSheet extends ItemSheet
{
	/** @override */
	static get defaultOptions()
	{
		return mergeObject(super.defaultOptions,
		{
			width: 620,
			height: 800,
			classes: ["lotc", "sheet", "item"]
			//tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "characteristics" }]
		});
	}

	/**
	 * Provides the data to the template when rendering the item sheet
	 *
	 * This is called when rendering the sheet, where it calls the base item class to organize, process, and prepare all item data for display.
	 *
	 * @returns {Object} data    Data given to the template when rendering
	 */
	getData()
	{
		const data = super.getData();
		
		data.config = CONFIG.LotC;
		data.characteristics = data.config.characteristics;
		//data.rollData = data.actor.getRollData();
		
		return data;
	}
	
	get template()
	{
		return `systems/lotc/templates/item-${this.item.type}-sheet.html`;
	}
	
	/*constructItemLists(data)
	{
		let items =
		{
			skills: data.items.filter(function(item) { return item.type == "skill" }),
			passiveAbilities: data.items.filter(function(item) { return item.type == "passive_ability" }),
			activeAbilities: data.items.filter(function(item) { return item.type == "active_ability" }),
			items: data.items.filter(function(item) { return item.type == "item" }),
			weapons: data.items.filter(function(item) { return item.type == "weapon" }),
			armours: data.items.filter(function(item) { return item.type == "armour" })
		};
		
		return items;
	}
	
	activateListeners(html)
	{
		super.activateListeners(html);
		
		html.find('.item-delete').click(this._onItemDelete.bind(this));
	}
	
	_getItemId(ev)
	{
		return $(ev.currentTarget).parents(".item").attr("data-item-id")
	}
	
	_onItemDelete(ev)
	{
		let li = $(ev.currentTarget).parents(".item"), itemId = li.attr("data-item-id");
		
		renderTemplate('systems/lotc/templates/dialogs/delete-item.html').then(html =>
		{
			new Dialog(
			{
				title: "Delete Confirmation",
				content: html,
				buttons:
				{
					Yes:
					{
						icon: '<span class="fa fa-check"></span>',
						label: "Yes",
						callback: async dlg =>
						{
							await this.actor.deleteEmbeddedDocuments("Item", [itemId]);
							li.slideUp(200, () => this.render(false));
						}
					},
					cancel:
					{
						icon: '<span class="fas fa-times"></span>',
						label: "Cancel"
					},
				},
				default: 'Yes'
			}).render(true)
		})
	}*/
}

Items.unregisterSheet("core", ItemSheet);
Items.registerSheet("LOTC", LotCItemSheet, {label: "Item Sheet", types: ["item", "weapon", "armour", "skill", "passive_ability", "active_ability", "rank"], makeDefault: true});
