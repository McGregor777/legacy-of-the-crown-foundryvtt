export default function()
{
	Hooks.on('init', () =>
	{
		Handlebars.registerHelper('equals', function(valueOne, valueTwo) { return valueOne === valueTwo });
	});
}