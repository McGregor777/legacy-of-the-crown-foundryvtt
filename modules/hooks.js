import * as Chat from './chat.js';

export default function()
{
	Hooks.on('renderChatLog', function(app, html, data)
	{
		html.on('click', 'button.test-parry', Chat.onParryTestButtonClick);
		html.on('click', 'button.test-dodge', Chat.onDodgeTestButtonClick);
	});

	Hooks.on('getChatLogEntryContext', Chat.addChatMessageContextOptions);
}