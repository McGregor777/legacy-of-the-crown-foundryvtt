import { Test }  from '../test.js';

/**
 * Provides the main Actor data computation and organization.
 *
 * LotCCharacter contains all the preparation data and methods used for preparing an actor: going through each Owned Item, preparing them for display based on characteristics.
 *
 * @see   LotCCharacterSheet - Character sheet class
 */
export default class LotCCharacter extends Actor
{
	/**
	 * Calculates simple dynamic data when actor is updated.
	 */
	prepareData()
	{
		this.prepareItems();
		this.calculateDerivedCharacteristics();
		super.prepareData();
	}
	
	prepareItems()
	{
		for(let item of this.items)
			item.prepareOwnedData();
	}
	
	/**
	 * Calculate all the Actor's Derived Characteristics depending on skills true values.
	 */
	calculateDerivedCharacteristics()
	{
		const skills = this.itemTypes.skill;
		let currentEncumbrance = 0;

		this.itemTypes.item.forEach(item => currentEncumbrance += item.system.encumbrance);
		this.itemTypes.weapon.forEach(weapon => currentEncumbrance += weapon.system.encumbrance);
		this.itemTypes.armour.forEach(armour => currentEncumbrance += armour.system.encumbrance);

		if(skills.length === 0)
			return;
		
		if(this.type != "creature")
		{
			let updates =
			{
				system:
				{
					derived_characteristics:
					{
						luck:
						{
							max: 2,
						},
						stress:
						{
							// Stress = Fortitude / 5 (floored)
							max: Math.floor(skills.find(skill => skill.name == game.i18n.localize("SKILL.Fortitude")).system.true_value / 5),
						},
						contamination:
						{
							// Contamination = Esoteric Resilience / 5 (floored)
							max: Math.floor(skills.find(skill => skill.name == game.i18n.localize("SKILL.EsotericResilience")).system.true_value / 5),
						},
						encumbrance:
						{
							// Encumbrance = Strength / 5 (floored)
							max: Math.floor(skills.find(skill => skill.name == game.i18n.localize("SKILL.Strength")).system.true_value / 5),
							value: currentEncumbrance
						},
						// Strike Force = Strength / 25 (floored) × 2
						strike_force: Math.floor(skills.find(skill => skill.name == game.i18n.localize("SKILL.Strength")).system.true_value / 25) * 2,
						// Esoteric Resistance = Esoteric Resilience / 25 (floored)
						esoteric_resistance: Math.floor(skills.find(skill => skill.name == game.i18n.localize("SKILL.EsotericResilience")).system.true_value / 25),
						// Speed = Athletics / 5 (floored) + 8
						speed: Math.floor(skills.find(skill => skill.name == game.i18n.localize("SKILL.Athletics")).system.true_value / 5) + 8,
						// Stamina = Vigour / 10 (floored)
						stamina: Math.floor(skills.find(skill => skill.name == game.i18n.localize("SKILL.Vigour")).system.true_value / 10),
						// Saves Initiative True Value in Actor's data for Initiative Rolls
						initiative: skills.find(skill => skill.name == game.i18n.localize("SKILL.Initiative")).system.true_value
					}
				}
			}
			
			if(this.type == "character")
			{
				// Health Points = Vitality / 5 (floored) + 5
				updates.system.derived_characteristics.health_points = { max: Math.floor(skills.find(skill => skill.name == game.i18n.localize("SKILL.Vitality")).system.true_value / 5) + 5, };
				// Physical Resistance = Resistance / 25 (floored)
				updates.system.derived_characteristics.physical_resistance = Math.floor(skills.find(skill => skill.name == game.i18n.localize("SKILL.Resistance")).system.true_value / 25);
			}
			
			this.update(updates);
		}
	}

	async stressTest()
	{
		Test(
			this,
			this.itemTypes.skill.find(skill => skill.name == game.i18n.localize("SKILL.Fortitude")),
			{ type: 'stress_test' }
		);
	}

	async contaminationTest()
	{
		Test(
			this,
			this.itemTypes.skill.find(skill => skill.name == game.i18n.localize("SKILL.EsotericResilience")),
			{ type: 'contamination_test' }
		);
	}
	
	/**
	 * Adds a new proficiency to the Actor's proficiency list.
	 */
	async addNewProficiency()
	{
		let html = await renderTemplate("/systems/lotc/templates/dialogs/create-proficiency-dialog.html", { weaponGroups: CONFIG.LotC.weaponGroups });
		
		let name = await new Promise((resolve, reject) =>
		{
			new Dialog(
			{
				title: game.i18n.localize("DIALOG.CreateProficiencyTitle"),
				content: html,
				buttons:
				{
					createButton:
					{
						label: game.i18n.localize("Create"),
						callback: html => resolve(html.find('[name="weapon_group_select"]').val())
					}
				},
				default: "createButton"
			}).render(true);
		});
		
		if(!this.system.proficiencies[name])
			this.update({system: {proficiencies: {[`${name}`]: {level: 1}}}});
		else
			ui.notifications.warn(`${this.name} already has a proficiency in this Weapon Group.`);
	}
	
	getProficiency(weaponGroupName)
	{
		let proficiency;
		
		for(let i = 0; i < Object.keys(this.system.proficiencies).length; i++)
		{
			if(this.system.proficiencies[`${i}`] == weaponGroupName)
				proficiency = this.system.proficiencies[i];
		}
		
		return proficiency;
	 }

	/**
	 * Deletes an existing proficiency from the Actor's proficiency list.
	 */
	async deleteProficiency(weaponGroupName)
	{
		this.update({system: {[`proficiencies.-=${weaponGroupName}`]: null}});
	}
	
	/*async _preCreate(data, options, user)
	{
		if(data._id)
			options.keepId = _keepID(data._id, this)
		
		await super._preCreate(data, options, user);

		// If the created actor has items (only applicable to duplicated actors) bypass the new actor creation logic
		if(data.items)
			return;

		let createData = {};
		createData.items = await this._getNewActorItems();

		// Set wounds, advantage, and display name visibility
		if(!data.token)
			mergeObject(createData,
			{
				token:
				{
					name: data.name,									// Set token name to actor name
					displayName: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,	// Default display name to be on owner hover
					displayBars: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,	// Default display bars to be on owner hover
					disposition: CONST.TOKEN_DISPOSITIONS.NEUTRAL,		// Default disposition to neutral
					vision: true,
					brightSight: 1,
					dimSight: 2
				}
			})
		else if(data.token)
			createData.token = data.token;

		// Set custom default token
		if(!data.img)
		{
			createData.img = "systems/wfrp4e/tokens/unknown.png";
			if(data.type == "vehicle")
				createData.img = "systems/wfrp4e/tokens/vehicle.png";
		}
		
		// Default characters to Link Data = true
		if(data.type == "character")
			createData.token.actorLink = true;
		
		console.log(createData);

		this.updateSource(createData);
	}*/
	
	static _keepID(id, document)
	{
		try
		{
			let compendium = !!document.pack;
			let world = !compendium;
			let collection;

			if(compendium)
			{
				let pack = game.packs.get(document.pack);
				collection = pack.index;
			}
			else if(world)
				collection = document.collection;

			if(collection.has(id))
			{
				ui.notifications.notify(`ID for ${document.name} already exists in collection. This Document has been given a unique ID.`)
				return false
			}
			
			else return true
		}
		catch(e)
		{
			console.error(e)
			return false
		}
	}

	/**
	 * Returns needed by new Actors: Basic Skills
	 */
	async _getNewActorItems()
	{
		let basicSkills = [];
		let skills = await game.packs.get("lotc.basic-skills").getDocuments();
		
		for(let sk of skills)
		{
			basicSkills.push(sk.toObject());
		}

		// Automatically add creature skills or basic skills depending on the Character's type
		if(this.type == "creature")
		{}
			//return basicSkills.concat(moneyItems)
		else
			return basicSkills;
			//return basicSkills.concat(moneyItems)
	}

	/**
	 * Universal card options for setup functions.
	 *
	 * The setup_____() functions all use the same cardOptions, just different templates. So this is a standardized helper function to maintain DRY code.
	 *
	 * @param {string} template	Fileptah to the template being used
	 * @param {string} title	Title of the Test to be displayed on the dialog and card
	 */
	_setupCardOptions(template, title)
	{
		let cardOptions =
		{
			speaker:
			{
				alias: this.token.name,
				actor: this._id,
			},
			title: title,
			template: template,
			flags: { img: this.token.randomImg ? this.img : this.token.img }
			// img to be displayed next to the name on the test card - if it's a wildcard img, use the actor image
		}

		// If the test is coming from a token sheet
		if(this.token)
		{
			cardOptions.speaker.token = this.token._id;
			cardOptions.speaker.scene = canvas.scene._id;
			cardOptions.flags.img = this.token.img; // Use the token image instead of the actor image
		}
		// Else if a linked actor, use the currently selected token's data if the actor id matches
		/*else 
		{
			let speaker = ChatMessage.getSpeaker()
			if(speaker.actor == this._id)
			{
				cardOptions.speaker.alias = speaker.alias;
				cardOptions.speaker.token = speaker.token;
				cardOptions.speaker.scene = speaker.scene;
				cardOptions.flags.img = speaker.token ? canvas.tokens.get(speaker.token).img : cardOptions.flags.img;;
			}
		}*/

		if(VideoHelper.hasVideoExtension(cardOptions.flags.img))
			game.video.createThumbnail(cardOptions.flags.img, { width: 50, height: 50 }).then(img => cardOptions.flags.img = img);

		return cardOptions;
	}
}