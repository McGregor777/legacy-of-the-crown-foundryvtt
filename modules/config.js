export const LotC = {};

LotC.characteristics =
{
	"physique": "CHARACTERISTIC.Physique",
	"constitution": "CHARACTERISTIC.Constitution",
	"agility": "CHARACTERISTIC.Agility",
	"intelligence": "CHARACTERISTIC.Intelligence",
	"willpower": "CHARACTERISTIC.Willpower",
	"charisma": "CHARACTERISTIC.Charisma"
}
	
LotC.classes =
{
	"alchemist": "CLASS.Alchemist",
	"assassin": "CLASS.Assassin",
	"cardmancer": "CLASS.Cardmancer",
	"crownsoldier": "CLASS.CrownSoldier",
	"gambler": "CLASS.Gambler",
	"mercenary": "CLASS.Mercenary",
	"outsider": "CLASS.Outsider",
	"ranger": "CLASS.Ranger",
	"rustphysician": "CLASS.RustPhysician",
	"technodeacon": "CLASS.Technodeacon"
}

LotC.weaponGroups =
{
	"onehanded": "WEAPONGROUP.OneHanded",
	"twohanded": "WEAPONGROUP.TwoHanded",
	"polearm": "WEAPONGROUP.Polearm",
	"brawling": "WEAPONGROUP.Brawling",
	"natural": "WEAPONGROUP.Natural",
	"shooting": "WEAPONGROUP.Shooting",
	"throwing": "WEAPONGROUP.Throwing",
	"pistol": "WEAPONGROUP.Pistol",
	"rifle": "WEAPONGROUP.Rifle",
	"sniper": "WEAPONGROUP.Sniper",
	"machinegun": "WEAPONGROUP.MachineGun",
	"explosive": "WEAPONGROUP.Explosive"
}

LotC.ranges =
{
	"pointblank": "RANGE.PointBlank",
	"veryshort": "RANGE.VeryShort",
	"short": "RANGE.Short",
	"medium": "RANGE.Medium",
	"long": "RANGE.Long",
	"verylong": "RANGE.VeryLong",
	"extreme": "RANGE.Extreme"
}

LotC.locations =
{
	"head": "LOCATION.Head",
	"body": "LOCATION.Body",
	"leftarm": "LOCATION.LeftArm",
	"rightarm": "LOCATION.RightArm",
	"leftleg": "LOCATION.LeftLeg",
	"rightleg": "LOCATION.RightLeg"
}

LotC.difficulties =
{
	"veryeasy": "DIFFICULTY.VeryEasy",
	"easy": "DIFFICULTY.Easy",
	"mildlyeasy": "DIFFICULTY.MildlyEasy",
	"average": "DIFFICULTY.Average",
	"complex": "DIFFICULTY.Complex",
	"hard": "DIFFICULTY.Hard",
	"extreme": "DIFFICULTY.Extreme"
}

LotC.difficultiesModifiers =
{
	"veryeasy": +30,
	"easy": +20,
	"mildlyeasy": +10,
	"average": 0,
	"complex": -10,
	"hard": -20,
	"extreme": -30
}