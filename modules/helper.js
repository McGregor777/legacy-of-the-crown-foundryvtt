/**
 * Determines if a Weapon is a ranged weapon, a close combat, or a fist fight weapon.
 * 
 * @param {Object} skill The Skill item being tested.
 * @param {Object} actor The Actor owning the Skill.
 * @param {Object} data	Specific settings.
 */
export function DetermineWeaponType(weapon)
{
    switch(weapon.system.group)
    {
        case 'shooting' || 'throwing' || 'pistol' || 'rifle' || 'sniper' || 'machinegun' || 'explosive':
            return 'ranged';
        case 'onehanded' || 'twohanded' || 'polearm':
            return 'close_combat';
        case 'brawling':
            return 'fist_fight';
    }
}