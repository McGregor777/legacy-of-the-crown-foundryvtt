import * as Helper from "../helper.js";
import * as Test from "../test.js";

export default class LotCItem extends Item
{
	chatTemplate = { "skill": "/systems/lotc/templates/chat/roll/skill-chat-card.html" };

	/**
	 * Calculates simple dynamic data when actor is updated.
	 */
	prepareData()
	{
		super.prepareData();
	}
	
	prepareOwnedData()
	{
		try
		{
			// Call `prepareOwned<Type>` function
			let functionName = `prepareOwned${this.type[0].toUpperCase() + this.type.slice(1, this.type.length)}`;
			if(this[`${functionName}`])
				this[`${functionName}`]();
		}
		catch(e)
		{
			console.error(`Something went wrong when preparing actor item ${this.name}: ${e}`);
		}
	}
	
	prepareOwnedSkill()
	{
		let createData = { system: { true_value: this.system.raw_value }}
		if(this.actor.type == "character")
			createData.system.true_value += this.actor.system.characteristics[`${this.system.associated_characteristic}`];

		this.update(createData);
	}

	/**
	 * Prepares a roll depending on the Item used.
	 * @param {any} actor
	 */
	async roll(actor)
	{
		switch(this.type)
		{
			case "skill":
				Test.Test(this.actor, this);
				break;
			case "weapon":
				let data = { title: this.name, type: 'attack', damages: this.system.damages };
				
				if(this.actor.type == "character")
				{
					let proficiency = this.actor.getProficiency(this.system.group);

					if(proficiency)
						data.modifier = proficiency.level * 10 - 10;
				}

				let skills = this.actor.itemTypes.skill;
				let skill = "";
				
				// Determine the Skill to test depending on the Weapon's Group
				switch(Helper.DetermineWeaponType(this))
				{
					case('ranged'):
						skill = skills.find(skill => skill.name == game.i18n.localize("SKILL.Shoot/Throw"));
						break;
					// Add the Actor's Strike Force to the Damages if the Weapon is not a ranged weapon
					case('close_combat'):
						data.damages += this.actor.system.derived_characteristics.strike_force;
						skill = skills.find(skill => skill.name == game.i18n.localize("SKILL.CloseCombat"));
						break;
					default:
						data.damages += this.actor.system.derived_characteristics.strike_force;
						skill = skills.find(skill => skill.name == game.i18n.localize("SKILL.Brawling"));
						break;
                }
				
				Test.Test(this.actor, skill, data);
				break;
		}
	}
	
	async _preCreate(data, options, user)
	{
		if(data._id && !this.isOwned)
			options.keepId = _keepID(data._id, this);
		
		await super._preCreate(data, options, user);
	}
	
	static _keepID(id, document)
	{
		try
		{
			let compendium = !!document.pack;
			let world = !compendium;
			let collection;

			if(compendium)
			{
				let pack = game.packs.get(document.pack);
				collection = pack.index;
			}
			else if(world)
				collection = document.collection;

			if(collection.has(id))
			{
				ui.notifications.notify(`ID for ${document.name} already exists in collection. This Document has been given a unique ID.`)
				return false
			}
			
			else return true
		}
		catch(e)
		{
			console.error(e)
			return false
		}
	}
}